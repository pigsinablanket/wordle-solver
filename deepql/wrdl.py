#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Uncomment and run cell to install dependancies
#!pip install tensorflow
#!pip install gym
#!pip install keras
#!pip install keras-rl2
#!pip install numpy


# Imports

# In[2]:


from gym import Env
from gym.spaces import Discrete, Box
import random
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.optimizers import Adam
from rl.agents import DQNAgent
from rl.policy import BoltzmannQPolicy
from rl.memory import SequentialMemory


# Enviroment definition

# In[3]:


class wrdl(Env):
    def __init__(self, wordlist):
        self.wordlist = wordlist
        self.actions = len(self.wordlist)-1
        self.action_space = Discrete(self.actions)
        self.observation_space = Box(low=np.array([0,0,0]), high=np.array([self.actions,5,5]), dtype=float)
        self.key = self.wordlist[random.randint(0,self.actions)]
        self.state = np.array([0,0,0])
        self.steps = 0
        self.results = []
        self.rewards = []
        self.rewardsum = 0
    def step(self, action):
        self.steps += 1
        guess = self.wordlist[action]
        green = 0
        yellow = 0
        reward = 0
        done = False
        if guess == self.key:
            self.results.append(self.steps)
            done = True
            reward = 100
            self.rewardsum += reward
            self.rewards.append(self.rewardsum)
    
        for i in range(0,4):
            if guess[i] == self.key[i]:
                green += 1
            elif guess[i] in self.key:
                yellow += 1
        self.state = np.array([action,yellow,green])
        if not done:
            reward += (2*yellow) + (5*green)
        self.rewardsum+= reward

        info = {}
        if self.steps == 100:
            done = True
            self.results.append('Fail')
            self.rewards.append(self.rewardsum)
        return self.state, reward, done, info
    def reset(self):
        self.key = self.wordlist[random.randint(0,self.actions)]
        self.state = np.array([0,0,0])
        self.steps = 0
        self.rewardsum = 0
        return self.state
    def getkey(self):
        return self.key
    def close(self):
        self.file = open('results.txt','w')
        for i in self.results:
            self.file.write(str(i)+'\n')
        self.file.close()
        self.file = open('rewards.txt','w')
        for i in self.rewards:
            self.file.write(str(i)+'\n')
        self.file.close()
    def render():
        pass


# In[4]:


def buildmodel(shape,actions):
    model = Sequential()
    model.add(Dense(32, activation='relu', input_shape=(1,3)))
    model.add(Dense(32, activation='relu'))
    model.add(Flatten())
    model.add(Dense(actions, activation='linear'))
    return model


# In[5]:


def buildagent(model, actions):
    policy = BoltzmannQPolicy()
    memory = SequentialMemory(limit=10000, window_length = 1)
    dqn = DQNAgent(model=model, memory=memory, policy=policy, 
                   nb_actions=actions, nb_steps_warmup=10, target_model_update= 1e-2)
    return dqn


# In[6]:


f = open("library",'r')
wordlist=f.read().splitlines()
f.close()


# In[7]:


env = wrdl(wordlist)


# In[8]:


shape = env.observation_space.shape
numact = env.action_space.n


# In[9]:


model = buildmodel(shape,numact)


# In[10]:


dqn = buildagent(model, numact)
dqn.compile(Adam(learning_rate=1e-3), metrics=['mae'])
dqn.fit(env, nb_steps=1000000, visualize=False, verbose=1)


# In[11]:


model.save('model')


# In[12]:


env.close()


# In[ ]:




