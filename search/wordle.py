#! /usr/bin/env python3

from functools import reduce
from statistics import stdev

numGuesses = 20

# Initialises dictionary with all 0s
def initialFitness(dictionary: list[str]) -> list[tuple[str,int]]:
    return list(zip(dictionary,[0]*len(dictionary)))

# The wordle game, returns [2,0,0,1] where 2 is right letter in right postion
# 0 is letter is not in word
# 1 is letter is in word but not in right position
def wordle(guess: str, finalState: str) -> list[int]:
    charMapping = lambda x, y: 2 if x == y else 1 if x in finalState else 0
    return list(map(charMapping,guess,finalState))

# Fitness of a word based on previous guess, 5 for right position, 1 for out of place
# 0 for not in word
def calcFitness(score: list[int], guess: str, word: str) -> int:
    # count 2s
    twos = reduce( lambda accum, t: ((5 if t[0]==t[1] else 0) if t[2]==2 else 0) + accum
                 , zip(guess,word,score)
                 , 0)
    # count 1s
    countNotAtIndex = lambda c, i: (guess[:i]+guess[i+1:]).count(c)
    ones = reduce( lambda accum, t: countNotAtIndex(t[0],t[2]) + accum if t[1] == 1 else 0
                 , zip(word,score,range(len(word)))
                 , 0)
    return ones+twos

# Sorts based on the fitness
def sortSearchSpace(searchSpace: list[tuple[str,int]]) -> list[tuple[str,int]]:
    return sorted(searchSpace, key=lambda x: x[1], reverse=True)

# Removes words from search spaces that contains words that contain characters not in word
def trimSearchSpace(guess: str,score: list[int],searchSpace: list[tuple[str,int]]) -> list[tuple[str,int]]:
    notInWord = list(map(lambda x: x[0], filter(lambda t: (t[1]==0) , zip(guess,score))))
    contains = lambda toFilter, word: len(set(toFilter) & set(word)) == 0
    return list(filter(lambda wordMap: contains(notInWord, wordMap[0]), searchSpace))

def searchLoop(searchSpace: list[tuple[str,int]], finalState: str, counter: int) -> int:
    word = searchSpace[0][0]
    print("Guessing word: " + word)
    if counter == 0:
        print("not found :(")
        return -1
    if word == finalState:
        print("you win :) found in: " + str(numGuesses-counter))
        return (numGuesses-counter)
    score = wordle(word, finalState)
    trimmedSearchSpace = trimSearchSpace(word,score,searchSpace)
    scoredSearchSpace = list(map(lambda x: (x[0],calcFitness(score,x[0],word)), trimmedSearchSpace))
    sortedSearchSpace = sortSearchSpace(scoredSearchSpace)
    return searchLoop(sortedSearchSpace, finalState, counter-1)

def main():
    searchSpace = initialFitness(open("dictionary.txt","r").read().splitlines())
    words = ["aces","thin","wrap","snow","pool","moms","luck","jazz","horn","duck"]
    results = list(map(lambda word: searchLoop(searchSpace,word,numGuesses),words))
    print(results)
    print("Avergae: " + str(sum(results)/len(results)))
    print("Std Dev: " + str(stdev(results)))

main()
